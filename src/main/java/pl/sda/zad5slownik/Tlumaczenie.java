package pl.sda.zad5slownik;

public class Tlumaczenie {
    String angielskie;
    String hiszpanskie;

    @Override
    public String toString() {
        if (hiszpanskie.equalsIgnoreCase("")&&angielskie.equalsIgnoreCase("")){
            return "Brak Tłumaczenia";
        }
        if (hiszpanskie.equalsIgnoreCase("")){
            return "Tlumaczenie " +
                    "angielskie='" + angielskie + '\'';
        }if (angielskie.equalsIgnoreCase("")){
            return "Tlumaczenie " +
                    "hiszpanskie='" + hiszpanskie + '\'';
        }
        return "Tlumaczenie " +
                "angielskie='" + angielskie + '\'' +
                ", hiszpanskie='" + hiszpanskie + '\'';
    }

    public Tlumaczenie(String angielskie, String hiszpanskie) {
        this.angielskie = angielskie;
        this.hiszpanskie = hiszpanskie;
    }
}
