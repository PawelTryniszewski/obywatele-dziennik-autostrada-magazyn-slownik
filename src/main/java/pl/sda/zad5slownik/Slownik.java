package pl.sda.zad5slownik;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Slownik {
    Map<String, Tlumaczenie> slownik = new HashMap<>();

    public void dodajTłumaczenie(String poPolsku, String poAngielsku,String pohiszpansku) {
        slownik.put(poPolsku, new Tlumaczenie(poAngielsku,pohiszpansku));
    }

    public String sprawdzTłumaczenie(String poPolsku) {
        if (slownik.get(poPolsku) != null) {
            return slownik.get(poPolsku).toString();
        } else
            return "Brak Tłumaczenia";
    }










//    Map<String, String> slownik = new HashMap<>();
//
//    public void dodajTłumaczenie(String poPolsku, String poAngielsku) {
//        slownik.put(poPolsku, poAngielsku);
//    }
//
//    public String sprawdzTłumaczenie(String poPolsku) {
//        if (slownik.get(poPolsku) != null) {
//            return slownik.get(poPolsku);
//        } else
//           return "Brak Tłumaczenia";
//    }
}
