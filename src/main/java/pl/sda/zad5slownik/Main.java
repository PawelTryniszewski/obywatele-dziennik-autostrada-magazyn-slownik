package pl.sda.zad5slownik;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        Slownik slownik = new Slownik();

        Scanner scanner = new Scanner(System.in);
        String komenda;
        do {
            komenda = scanner.nextLine();
            switch (komenda) {
                case "dodaj":
                    System.out.println("pisz");
                    slownik.dodajTłumaczenie(scanner.nextLine(), scanner.nextLine(),scanner.nextLine());
                    break;
                case "tlumacz":
                    System.out.println(slownik.sprawdzTłumaczenie(scanner.nextLine()));
                    break;
                default:
                    if (!komenda.equalsIgnoreCase("quit")) {
                        System.out.println("zla komenda");
                    }

            }

        } while (!komenda.equalsIgnoreCase("quit"));

    }
}
