package pl.sda.zad2dziennikszkolny;

public class DaneStudenta {
    String imie; String nazwisko; String numerIndeksu;
    OcenyStudenta ocenyStudenta = new OcenyStudenta();

    public String getImie() {
        return imie;
    }

    public void setImie(String imie) {
        this.imie = imie;
    }

    public String getNazwisko() {
        return nazwisko;
    }

    public void setNazwisko(String nazwisko) {
        this.nazwisko = nazwisko;
    }

    public String getNumerIndeksu() {
        return numerIndeksu;
    }

    public void setNumerIndeksu(String numerIndeksu) {
        this.numerIndeksu = numerIndeksu;
    }

    public OcenyStudenta getOcenyStudenta() {
        return ocenyStudenta;
    }

    public void setOcenyStudenta(OcenyStudenta ocenyStudenta) {
        this.ocenyStudenta = ocenyStudenta;
    }

    public DaneStudenta(String imie, String nazwisko, String numerIndeksu) {
        this.imie = imie;
        this.nazwisko = nazwisko;
        this.numerIndeksu = numerIndeksu;

    }
}
