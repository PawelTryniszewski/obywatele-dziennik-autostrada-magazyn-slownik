package pl.sda.zad3autostrada;

import java.time.Duration;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.Period;
import java.util.ArrayList;
import java.util.List;

public class Highway {

    List<VehicleInfo> highway = new ArrayList<>();

    public void vehicleEntry(String numer_rejestracyjny, CarType type) {
        highway.add(new VehicleInfo(numer_rejestracyjny, type, LocalTime.now()));
        System.out.println("new vehicle enter");
    }

    public void vehicleLeave(String numer_rejestracyjny) {
        for (int i = 0; i < highway.size(); i++) {
            if (highway.get(i).getNrRejestracyjny().equalsIgnoreCase(numer_rejestracyjny)) {
                double cenaPrzejazdu = 0;
                highway.get(i).setDataWjazdu(highway.get(i).getDataWjazdu().minusMinutes(66));
                Duration duration = Duration.between(highway.get(i).getDataWjazdu(), LocalTime.now());
                if (highway.get(i).getTypPojazdu() == CarType.CAR
                        || highway.get(i).getTypPojazdu() == CarType.MOTORCYCLE) {
                    cenaPrzejazdu = duration.toMinutes() * 0.08;
                } else if (highway.get(i).getTypPojazdu() == CarType.TRUCK) {
                    cenaPrzejazdu = duration.toMinutes() * 0.18;
                }
                highway.remove(i);
                System.out.printf("Cena Przejazdu %.2f\n" , cenaPrzejazdu);
            }
        }
    }

    public void searchVehicle(String numer_rejestracyjny) {
        boolean isPresent = false;
        for (VehicleInfo s : highway) {
            if (s.getNrRejestracyjny().equalsIgnoreCase(numer_rejestracyjny)) {
                System.out.println(s);
                isPresent = true;
            }
        }
        if (!isPresent) {
            System.out.println("cannot find");
        }
    }
}
