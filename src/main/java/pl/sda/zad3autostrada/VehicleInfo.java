package pl.sda.zad3autostrada;

import java.time.LocalDateTime;
import java.time.LocalTime;

public class VehicleInfo {
    String nrRejestracyjny; CarType typPojazdu; LocalTime dataWjazdu;

    public String getNrRejestracyjny() {
        return nrRejestracyjny;
    }

    public void setNrRejestracyjny(String nrRejestracyjny) {
        this.nrRejestracyjny = nrRejestracyjny;
    }

    public CarType getTypPojazdu() {
        return typPojazdu;
    }

    public void setTypPojazdu(CarType typPojazdu) {
        this.typPojazdu = typPojazdu;
    }

    public LocalTime getDataWjazdu() {
        return dataWjazdu;
    }

    public void setDataWjazdu(LocalTime dataWjazdu) {
        this.dataWjazdu = dataWjazdu;
    }

    public VehicleInfo(String nrRejestracyjny, CarType typPojazdu, LocalTime dataWjazdu) {
        this.nrRejestracyjny = nrRejestracyjny;
        this.typPojazdu = typPojazdu;
        this.dataWjazdu = dataWjazdu;
    }

    @Override
    public String toString() {
        return "VehicleInfo{" +
                "nrRejestracyjny='" + nrRejestracyjny + '\'' +
                ", typPojazdu=" + typPojazdu +
                ", godzWjazdu=" + dataWjazdu +
                '}';
    }
}
