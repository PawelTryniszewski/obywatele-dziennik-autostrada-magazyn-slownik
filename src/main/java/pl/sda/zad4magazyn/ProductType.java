package pl.sda.zad4magazyn;

public enum ProductType {
    FOOD, INDUSTRIAL, ALCOHOL;
}
