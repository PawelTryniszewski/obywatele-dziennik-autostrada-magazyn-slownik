package pl.sda.zad4magazyn;

public class Main {
    public static void main(String[] args) {
        Magazine nowy = new Magazine();

        nowy.dodajProdukt("woda",2.2,ProductType.FOOD,ProductClass.HIGH);
        nowy.dodajProdukt("ser",2.2,ProductType.FOOD,ProductClass.HIGH);
        nowy.dodajProdukt("mieso",2.2,ProductType.FOOD,ProductClass.HIGH);
        nowy.dodajProdukt("mleko",2.2,ProductType.FOOD,ProductClass.HIGH);
        nowy.dodajProdukt("mleko",2.2,ProductType.FOOD,ProductClass.LOW);
        nowy.dodajProdukt("mleko",2.2,ProductType.FOOD,ProductClass.LOW);
        nowy.dodajProdukt("mleko",2.2,ProductType.FOOD,ProductClass.LOW);
        nowy.dodajProdukt("mleko",2.2,ProductType.FOOD,ProductClass.LOW);
        nowy.wypiszPoTypie(ProductType.FOOD);
        nowy.wypiszPoKlasie(ProductClass.LOW);
    }
}
