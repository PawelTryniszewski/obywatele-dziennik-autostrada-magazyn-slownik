package pl.sda.zad4magazyn;

import java.util.*;

public class Magazine {

    Map<ProductClass, List<Product>> stanMagazynuPoKlasie = new HashMap<>();
    Map<ProductType, List<Product>> stanMagazynuPoTypie = new HashMap<>();

    public void dodajProdukt(String nazwa, Double cena, ProductType productType, ProductClass productClass) {
        Product product = new Product(nazwa, cena, productType, productClass);
        List<Product> productList = stanMagazynuPoKlasie.get(productClass);
        List<Product> productList1 = stanMagazynuPoTypie.get(productType);
        if (productList == null) {
            productList = new ArrayList<>();

        }
        if (productList1 == null) {
            productList1= new ArrayList<>();
        }
        productList.add(product);
        productList1.add(product);
        stanMagazynuPoKlasie.put(productClass, productList);
        stanMagazynuPoTypie.put(productType, productList1);
    }

    public void wypiszPoTypie(ProductType type) {
        System.out.println(stanMagazynuPoTypie.get(type));

    }

    public void wypiszPoKlasie(ProductClass productClass) {
        System.out.println(stanMagazynuPoKlasie.get(productClass));

    }

}
