package pl.sda.zad4magazyn;

public class Product {
    String nazwa;
    Double cena;
    ProductType type;
    ProductClass productClass;

    @Override
    public String toString() {
        return "Product{" +
                "nazwa='" + nazwa + '\'' +
                ", cena=" + cena +
                ", type=" + type +
                ", productClass=" + productClass +
                '}';
    }

    public Product(String nazwa, Double cena, ProductType type, ProductClass productClass) {
        this.nazwa = nazwa;
        this.cena = cena;
        this.type = type;
        this.productClass = productClass;
    }
}
