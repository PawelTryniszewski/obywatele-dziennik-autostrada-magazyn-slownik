package pl.sda.zad1obywatele;

import java.util.*;
import java.util.stream.Collectors;

public class RejestrObywateli {
    Map<String,Obywatel> mapaObywateli= new HashMap();

    public void dodajObywatela(String pesel, String imie, String nazwisko){
        Obywatel temp = new Obywatel(imie,nazwisko,pesel);
        mapaObywateli.put(pesel,temp);

    }

    public List<Obywatel> znajdźObywateliUrodzonychPrzed(int rok){
       return mapaObywateli.values().stream().filter(obywatel -> rok>Integer.parseInt(obywatel.getPesel().
       substring(0,2))).collect(Collectors.toList());
    }

    public List<Obywatel> znajdźObywateliZRokuZImieniem(int rok, String imie){
        List<Obywatel> listaObywateli = mapaObywateli.values().stream().filter(x->Integer.valueOf(x.getPesel().substring(0,2))
                ==Integer.valueOf(String.valueOf(rok).substring(2,4))).collect(Collectors.toList());
        List<Obywatel> wynik = listaObywateli.stream().filter(x->x.getImie().equals(imie)).collect(Collectors.toList());
        return wynik;
    }
    public List<Obywatel> znajdźObywatelaPoNazwisku(String nazwisko){
        return mapaObywateli.values().stream().filter(x->x.getNazwisko().equalsIgnoreCase(nazwisko)).collect(Collectors.toList());
    }

    public Optional<Obywatel> znajdźObywatelaPoPeselu(String pesel){
        return Optional.ofNullable(mapaObywateli.get(pesel));
       // return mapaObywateli.values().stream().filter(x->x.getPesel().equals(pesel)).findAny();
    }




}
